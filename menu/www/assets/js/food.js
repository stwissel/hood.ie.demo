// initialize Hoodie
var hoodie = new Hoodie();

(function($, hoodie) {
  'use strict';

  function addFoodToTable(newFood) {
    var newRow = '<tr data-food-id="'
      + newFood.id + '"><td>'
      + newFood.name + '</td><td>'
      + newFood.color + '</td><td>'
      + newFood.smell + '</td><td>'
      + '<button type="button" class="btn btn-danger">Delete</button></td></tr>';

    $('#food-table tbody').append(newRow);
  }

  // initial load of all todo items from the store
  hoodie.store.findAll('food').then(function(allTodos) {
    allTodos.forEach(addFoodToTable);
  });

  hoodie.store.on('add:food', addFoodToTable);

  hoodie.store.on('remove:food', function(foodItem){
    $('#food-table tr[data-food-id="' + foodItem.id + '"]').remove();
  });

  $('.create-food').on('click', function() {
    var $form = $.modalForm({
      fields: ['name', 'color', 'smell'],
      submit: 'Create Food'
    });

    $form.on('submit', function createFood(event, inputs) {
      var $modal = $(event.target),
        type = 'food',
        food = {
          name: inputs.name,
          color: inputs.color,
          smell: inputs.smell
        },
        magic = hoodie.store.add(type, food);

      magic.done(function() {
        $modal.find('.alert').remove();
        $modal.modal('hide');
      });
      magic.fail(function(error) {
        $modal.find('.alert').remove();
        $modal.trigger('error', error);
      });
    });
  });

  $('#food-table tbody').on('click', 'button.btn-danger', function() {
    hoodie.store.remove('food', $(this).parent().parent().data('food-id'));
    return false;
  });

  hoodie.account.on('signout', function(){
    $('#food-table tbody').empty();
  });

})(window.jQuery, hoodie);